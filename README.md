# DataConnect-NodeRED

Le document .json contient un ensemble de fonctions Node-RED, directement importables dans un espace de travail, 
pour se connecter aux API "bac à sable" de DataConnect (données du compteur Linky) mises à disposition par Enedis.

## Licence
Code produit lors du hackathon "DataConnect" des Consometers en avril 2019 : https://wiki.consometers.org/doku.php?id=projets:dataconnect
Placé sous Licence EUPL 1.2 (voir https://eupl.eu/1.2/fr/)
Auteur : Jaxom <jaxom@tchack.xyz>
Mise à jour : Mai 2020 - passage en v4

## Dépendances
Ce code utilise la bibliothèque complémentaire "nodered-dashboard".

## Fonction et limites
Ces deux Flows gèrent l'authentification sur l'API DataConnect et la requête de données dans la version "bac à sable".
Voir https://datahub-enedis.fr/data-connect/

Les données d'identification sont stockées en variables globales.
Un seul token est géré à la fois, chaque demande efface le précédent.
Les données sont directement traitées et envoyées au module de tracé de courbes.
Chaque nouvelle demande de courbe efface la précédente, les superpositions ne sont pas gérées.
Les trous de données ne sont pas gérés.

## Usage
1. Placer les différents identifiants obtenus auprès d'Enedis dans le bloc fonction "IDENTIFIANTS"
2. Déployer les Flows et se rendre sur la page web "~/ui"
3. Choisir un type de client
4. Cliquer sur le bouton "Login" pour obtenir un token
5. Choisir les dates de l'intervalle de requête et la nature de la requête
6. Cliquer sur "Requete Courbes" pour émettre la requête à l'API DataConnect
7. Observer les courbes

## Type de Clients
Les 10 types de clients du bac à sable répondent selon [cette page](https://datahub-enedis.fr/data-connect/ressources/decouvrir/).
